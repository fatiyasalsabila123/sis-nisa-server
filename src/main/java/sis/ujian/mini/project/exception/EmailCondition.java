package sis.ujian.mini.project.exception;

public class EmailCondition extends RuntimeException {
    public EmailCondition(String message) {
        super(message);
    }
}
