package sis.ujian.mini.project.model;

import javax.persistence.*;

@Entity
@Table(name = "tindakan")
public class Tindakan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "namaTindakan")
    private String NamaTindakan;

    public Tindakan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaTindakan() {
        return NamaTindakan;
    }

    public void setNamaTindakan(String namaTindakan) {
        NamaTindakan = namaTindakan;
    }
}
