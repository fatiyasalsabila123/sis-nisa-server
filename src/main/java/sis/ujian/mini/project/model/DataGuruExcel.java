package sis.ujian.mini.project.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class DataGuruExcel {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = {"nama guru",  "tempat lahir","tanggal lahir","alamat"};
    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static ByteArrayInputStream dataGuruToExcel(List<DataGuru> gurus) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }

            int rowIdx = 1;
            for (DataGuru dataGuru : gurus) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(dataGuru.getNama());
                row.createCell(1).setCellValue(dataGuru.getTempatLahir());
                row.createCell(2).setCellValue(dataGuru.getTanggalLahir());
                row.createCell(3).setCellValue(dataGuru.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    public static List<DataGuru> excelToGuru(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<DataGuru> guruList = new ArrayList<DataGuru>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                DataGuru dataGuru = new DataGuru();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            dataGuru.setNama(currentCell.getStringCellValue());
                            break;
                        case 1:
                            dataGuru.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 2:
                            dataGuru.setTanggalLahir(currentCell.getStringCellValue());
                            break;
                        case 3:
                            dataGuru.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    guruList.add(dataGuru);
                    cellIdx++;

                }

            }
            workbook.close();
            return guruList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

}
