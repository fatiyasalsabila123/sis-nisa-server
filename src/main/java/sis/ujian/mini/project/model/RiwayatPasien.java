package sis.ujian.mini.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "riwayat_pasien")
public class RiwayatPasien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "namaPasien")
    private String NamaPasien;

    @Column(name = "statusPasien")
    private String StatusPasien;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "Tanggal_JamPeriksa")
    private Date TanggalJamPeriksa;

    public RiwayatPasien() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPasien() {
        return NamaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        NamaPasien = namaPasien;
    }

    public String getStatusPasien() {
        return StatusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        StatusPasien = statusPasien;
    }

    public Date getTanggalJamPeriksa() {
        return TanggalJamPeriksa;
    }

    public void setTanggalJamPeriksa(Date tanggalJamPeriksa) {
        TanggalJamPeriksa = tanggalJamPeriksa;
    }
}
