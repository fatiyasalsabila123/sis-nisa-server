package sis.ujian.mini.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "data")
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne
//    @JoinColumn(name = "guru_id")
//    private Guru guruId;

    @ManyToOne
    @JoinColumn(name = "siswa_id")
    private Registrasi siswaId;

    @Column(name = "username")
    private String username;

    @Column(name = "jabatan")
    private String jabatan;

    @Column(name = "kelas")
    private String kelas;

    @Column(name = "status")
    private String status;

    @Column(name = "tempat")
    private String tempat;

    @Column(name = "tanggalLahir")
    private String tanggalLahir;

    @Column(name = "alamat")
    private String alamat;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "waktu", updatable = false)
    private Date waktu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public Registrasi getSiswaId() {
        return siswaId;
    }

    public void setSiswaId(Registrasi siswaId) {
        this.siswaId = siswaId;
    }
}
