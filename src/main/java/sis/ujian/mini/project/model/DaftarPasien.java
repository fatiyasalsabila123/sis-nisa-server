package sis.ujian.mini.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "daftar_pasien")
public class DaftarPasien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Registrasi userId;

    @OneToOne
    @JoinColumn(name = "nama_pasien")
    private Data namaPasien;

    @Column(name = "status_pasien")
    private String statusPasien;

    @Column(name = "keluhan")
    private String keluhan;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "penyakit_id")
    private Diagnosa penyakitId;

    @ManyToOne
    @JoinColumn(name = "penanganan_id")
    private Penanganan penangananId;

    @ManyToOne
    @JoinColumn(name = "tindakan_id")
    private Tindakan tindakanId;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "waktu", updatable = false)
    private Date waktu;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public String getStatusPasien() {
        return statusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        this.statusPasien = statusPasien;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Diagnosa getPenyakitId() {
        return penyakitId;
    }

    public void setPenyakitId(Diagnosa penyakitId) {
        this.penyakitId = penyakitId;
    }

    public Penanganan getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(Penanganan penangananId) {
        this.penangananId = penangananId;
    }

    public Tindakan getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Tindakan tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public Data getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(Data namaPasien) {
        this.namaPasien = namaPasien;
    }

    public Registrasi getUserId() {
        return userId;
    }

    public void setUserId(Registrasi userId) {
        this.userId = userId;
    }
}
