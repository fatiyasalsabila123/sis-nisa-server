package sis.ujian.mini.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sis.ujian.mini.project.model.Data;

import java.util.List;

@Repository
public interface DataRepository extends JpaRepository<Data, Long> {

    @Query(value = "SELECT * FROM data WHERE siswa_id = ?1", nativeQuery = true)
    List<Data> getAllUserId(Long userId);

    @Query(value = "SELECT * FROM data WHERE status LIKE CONCAT ('guru') AND siswa_id = ?1", nativeQuery = true)
    List<Data> getAllGuru( Long userId);

    @Query(value = "SELECT * FROM data WHERE status LIKE CONCAT ('siswa') AND siswa_id = ?1", nativeQuery = true)
    List<Data> getAllSiswa(Long userId);

    @Query(value = "SELECT * FROM data WHERE status LIKE CONCAT ('karyawan') AND siswa_id = ?1", nativeQuery = true)
    List<Data> getAllKaryawan(Long userId);


}
