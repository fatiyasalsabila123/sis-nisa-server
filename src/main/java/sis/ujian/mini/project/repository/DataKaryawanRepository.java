package sis.ujian.mini.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sis.ujian.mini.project.model.DataKaryawan;

public interface DataKaryawanRepository extends JpaRepository<DataKaryawan, Long> {
}
