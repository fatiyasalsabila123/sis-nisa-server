package sis.ujian.mini.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sis.ujian.mini.project.model.DataSiswa;

public interface DataSiswaRepository extends JpaRepository<DataSiswa, Long> {
}
