package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.DataGuru;

import java.util.List;
import java.util.Map;

public interface DataGuruService {

    DataGuru addguru(DataGuru dataGuru); //method add untuk mengepost

    DataGuru getguruById(Long id); // method untuk melihat sesuaiid yg dicari

    List<DataGuru> getAllguru(); // method untuk melihat semua data

    DataGuru editguruById(Long id, DataGuru dataGuru); // method untuk mengedit data sesuai id

    Map<String, Boolean> deleteguruById(Long id); // method untuk mendelete data sesaui data
}
