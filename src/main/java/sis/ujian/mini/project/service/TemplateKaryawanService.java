package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.DataKaryawan;
import sis.ujian.mini.project.model.DataSiswa;
import sis.ujian.mini.project.model.Template;
import sis.ujian.mini.project.model.TemplateKaryawan;
import sis.ujian.mini.project.repository.DataKaryawanRepository;
import sis.ujian.mini.project.repository.DataSiswaRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateKaryawanService {

    @Autowired
    DataKaryawanRepository dataKaryawanRepository;


    public ByteArrayInputStream pExcell() {
        List<DataKaryawan> karyawanList = dataKaryawanRepository.findAll();
        ByteArrayInputStream in = TemplateKaryawan.templateKarToExcel(karyawanList);
        return in;
    }
}
