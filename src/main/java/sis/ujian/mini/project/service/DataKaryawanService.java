package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.DataKaryawan;

import java.util.List;
import java.util.Map;

public interface DataKaryawanService {

    DataKaryawan addKar(DataKaryawan dataKaryawan); //method add untuk mengepost

    DataKaryawan getKarById(Long id); // method untuk melihat sesuaiid yg dicari

    List<DataKaryawan> getAllKar(); // method untuk melihat semua data

    DataKaryawan editKarById(Long id, DataKaryawan dataKaryawan); // method untuk mengedit data sesuai id

    Map<String, Boolean> deleteKarById(Long id); // method untuk mendelete data sesaui data
}
