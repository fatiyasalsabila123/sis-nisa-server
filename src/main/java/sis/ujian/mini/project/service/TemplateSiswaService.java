package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.DataSiswa;
import sis.ujian.mini.project.model.Template;
import sis.ujian.mini.project.repository.DataSiswaRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateSiswaService {

    @Autowired
    DataSiswaRepository dataSiswaRepository;


    public ByteArrayInputStream pExcell() {
        List<DataSiswa> siswasList = dataSiswaRepository.findAll();
        ByteArrayInputStream in = Template.templateToExcel(siswasList);
        return in;
    }
}
