package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.DataGuru;
import sis.ujian.mini.project.model.DataKaryawan;
import sis.ujian.mini.project.model.TemplateGuru;
import sis.ujian.mini.project.model.TemplateKaryawan;
import sis.ujian.mini.project.repository.DataGuruRepository;
import sis.ujian.mini.project.repository.DataKaryawanRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateGuruService {

    @Autowired
    DataGuruRepository dataGuruRepository;


    public ByteArrayInputStream pExcell() {
        List<DataGuru> guruList = dataGuruRepository.findAll();
        ByteArrayInputStream in = TemplateGuru.templateGuruToExcel(guruList);
        return in;
    }
}
