package sis.ujian.mini.project.service;

import sis.ujian.mini.project.Dto.PasienDto;
import sis.ujian.mini.project.Dto.PasienGuruDto;
import sis.ujian.mini.project.Dto.PasienKaryawanDto;
import sis.ujian.mini.project.Dto.PasienSiswaDto;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.DataSiswa;

import java.util.List;
import java.util.Map;

public interface DataService {

//    Data adddata(Data data); //method add untuk mengepost
//
    Data getdataById(Long id); // method untuk melihat sesuaiid yg dicari
//
//    List<Data> getAlldata(); // method untuk melihat semua data
//
//    List<Data>findByRole(String Status);
//
//    Data editdataById(Long id, Data data); // method untuk mengedit data sesuai id

    Map<String, Boolean> deletedataById(Long id); // method untuk mendelete data sesaui data

    List<Data> allGuru (Long userId);

    List<Data> allSiswa (Long userId);

    List<Data> allKaryawan (Long userId);

    List<Data> allSiswaId(Long userId);

    Data addPasien (PasienDto pasienDto);

    Data addSiswa (PasienSiswaDto pasienSiswaDto);

    Data addKaryawan (PasienDto pasienDto);

    Data editPasien (Long id, PasienDto pasienDto);

    Data editSiswa (Long id, PasienSiswaDto pasienSiswaDto);

    Data editKaryawan (Long id, PasienDto pasienDto);
}
