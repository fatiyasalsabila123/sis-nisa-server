
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.*;
import sis.ujian.mini.project.repository.DataGuruRepository;
import sis.ujian.mini.project.repository.DataKaryawanRepository;
import sis.ujian.mini.project.repository.DataSiswaRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class KaryawanExcelService {

    @Autowired
    DataKaryawanRepository dataKaryawanRepository;

    public ByteArrayInputStream loadKaryawan() {
        List<DataKaryawan> karyawans = dataKaryawanRepository.findAll();
        ByteArrayInputStream in = DataKaryawanExcel.dataKaryawanToExcel(karyawans);
        return in;
    }

    public void saveKaryawan(MultipartFile file) {
        try {
            List<DataKaryawan> karyawans = DataKaryawanExcel.excelToKaryawan(file.getInputStream());
            dataKaryawanRepository.saveAll(karyawans);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
