
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.DataGuru;
import sis.ujian.mini.project.model.DataGuruExcel;
import sis.ujian.mini.project.repository.DataGuruRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class GuruExcelService {

    @Autowired
    DataGuruRepository dataGuruRepository;

    public ByteArrayInputStream loadGuru() {
        List<DataGuru> gurus = dataGuruRepository.findAll();
        ByteArrayInputStream in = DataGuruExcel.dataGuruToExcel(gurus);
        return in;
    }

    public void saveGuru(MultipartFile file) {
        try {
            List<DataGuru> gurus = DataGuruExcel.excelToGuru(file.getInputStream());
            dataGuruRepository.saveAll(gurus);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
