
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.DataSiswa;
import sis.ujian.mini.project.model.DataSiswaExcel;
import sis.ujian.mini.project.repository.DataSiswaRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class SiswaExcelService {

    @Autowired
    DataSiswaRepository dataSiswaRepository;

    public ByteArrayInputStream loadSiswa() {
        List<DataSiswa> siswas = dataSiswaRepository.findAll();
        ByteArrayInputStream in = DataSiswaExcel.dataSiswaToExcel(siswas);
        return in;
    }

    public void saveSiswa(MultipartFile file) {
        try {
            List<DataSiswa> siswas = DataSiswaExcel.excelToSiswa(file.getInputStream());
            dataSiswaRepository.saveAll(siswas);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
