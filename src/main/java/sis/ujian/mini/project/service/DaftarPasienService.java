package sis.ujian.mini.project.service;

import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.model.DaftarPasien;

import java.util.List;
import java.util.Map;

public interface DaftarPasienService {
    DaftarPasien addpasien(PeriksaPasienDto periksaPasienDto); //method add untuk mengepost

    DaftarPasien getpasienById(Long id); // method untuk melihat sesuaiid yg dicari

    List<DaftarPasien> getAllpasien(); // method untuk melihat semua data

    DaftarPasien editpasienById(Long id, TanganiDto tanganiDto); // method untuk mengedit data sesuai id

    Map<String, Boolean> deletepasienById(Long id); // method untuk mendelete data sesaui data
}
