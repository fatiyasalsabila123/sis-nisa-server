package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.DataSiswa;

import java.util.List;
import java.util.Map;

public interface DataSiswaService {

    DataSiswa addsis(DataSiswa dataSiswa); //method add untuk mengepost

    DataSiswa getsisById(Long id); // method untuk melihat sesuaiid yg dicari

    List<DataSiswa> getAllsis(); // method untuk melihat semua data

    DataSiswa editsisById(Long id, DataSiswa dataSiswa); // method untuk mengedit data sesuai id

    Map<String, Boolean> deletesisById(Long id); // method untuk mendelete data sesaui data
}
