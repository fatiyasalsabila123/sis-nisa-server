package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.Penanganan;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.PenangananService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/penanganan")
@CrossOrigin(origins = "http://localhost:3006")
public class PenangananController {

    @Autowired
    PenangananService penangananService;

    @PostMapping
    public CommonResponse<Penanganan> addpena(@RequestBody Penanganan penanganan) {
        return ResponseHelper.ok(penangananService.addpena(penanganan));
    }

    @GetMapping("/{id}")
    public CommonResponse<Penanganan> getpenaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.getpenaById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Penanganan> editpenaById(@PathVariable("id") Long id,@RequestBody Penanganan penanganan) {
        return ResponseHelper.ok(penangananService.editpenaById(id, penanganan));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletepenaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.deletepenaById(id));
    }

    @GetMapping("/all-penanganan")
    public CommonResponse<List<Penanganan>> getAllpena() {
        return ResponseHelper.ok(penangananService.getAllpena());
    }
}
