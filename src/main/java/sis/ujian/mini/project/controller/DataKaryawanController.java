package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.DataKaryawan;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DataKaryawanService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/data-karyawan")
@CrossOrigin(origins = "http://localhost:3006")
public class DataKaryawanController {
    @Autowired
    DataKaryawanService dataKaryawanService;

    @PostMapping
    public CommonResponse<DataKaryawan> addKar(@RequestBody DataKaryawan dataKaryawan) {
        return ResponseHelper.ok(dataKaryawanService.addKar(dataKaryawan));
    }

    @GetMapping("/{id}")
    public CommonResponse<DataKaryawan> getKarById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataKaryawanService.getKarById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DataKaryawan> editKarById(@PathVariable("id") Long id,@RequestBody DataKaryawan dataKaryawan) {
        return ResponseHelper.ok(dataKaryawanService.editKarById(id, dataKaryawan));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteKarById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataKaryawanService.deleteKarById(id));
    }

    @GetMapping("/all-karyawan")
    public CommonResponse<List<DataKaryawan>> getAllKar() {
        return ResponseHelper.ok(dataKaryawanService.getAllKar());
    }
}
