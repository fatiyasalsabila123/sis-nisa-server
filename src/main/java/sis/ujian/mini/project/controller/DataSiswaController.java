package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.DataSiswa;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DataSiswaService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/data-siswa")
@CrossOrigin(origins = "http://localhost:3006")
public class DataSiswaController {

    @Autowired
    DataSiswaService dataSiswaService;

    @PostMapping
    public CommonResponse<DataSiswa> addsis(@RequestBody DataSiswa dataSiswa) {
        return ResponseHelper.ok(dataSiswaService.addsis(dataSiswa));
    }

    @GetMapping("/{id}")
    public CommonResponse<DataSiswa> getsisById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataSiswaService.getsisById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DataSiswa> editsisById(@PathVariable("id") Long id,@RequestBody DataSiswa dataSiswa) {
        return ResponseHelper.ok(dataSiswaService.editsisById(id, dataSiswa));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletesisById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataSiswaService.deletesisById(id));
    }

    @GetMapping("/all-siswa")
    public CommonResponse<List<DataSiswa>> getAllsis() {
        return ResponseHelper.ok(dataSiswaService.getAllsis());
    }
}
