package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.model.DaftarPasien;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DaftarPasienService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/daftar-pasien")
@CrossOrigin(origins = "http://localhost:3006")
public class DaftarPasienController {

    @Autowired
    DaftarPasienService daftarPasienService;

    @PostMapping
    public CommonResponse<DaftarPasien> addpasien(@RequestBody PeriksaPasienDto daftarPasien) {
        return ResponseHelper.ok(daftarPasienService.addpasien(daftarPasien));
    }

    @GetMapping("/{id}")
    public CommonResponse<DaftarPasien> getpasienById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarPasienService.getpasienById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DaftarPasien> editpasienById(@PathVariable("id") Long id,@RequestBody TanganiDto tanganiDto) {
        return ResponseHelper.ok(daftarPasienService.editpasienById(id, tanganiDto));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletepasienById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarPasienService.deletepasienById(id));
    }

    @GetMapping("/all-pasien")
    public CommonResponse<List<DaftarPasien>> getAllpasien() {
        return ResponseHelper.ok(daftarPasienService.getAllpasien());
    }

}
