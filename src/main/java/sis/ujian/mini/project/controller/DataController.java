package sis.ujian.mini.project.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.Dto.PasienDto;
import sis.ujian.mini.project.Dto.PasienKaryawanDto;
import sis.ujian.mini.project.Dto.PasienSiswaDto;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DataService;

import java.util.List;


@RestController
@RequestMapping("/data")
@CrossOrigin(origins = "http://localhost:3006")
public class DataController {

    @Autowired
    DataService dataService;

    @Autowired
    ModelMapper modelMapper;


    @GetMapping("/{id}") // melihat data sesuai id
    public CommonResponse<Data> getId (@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataService.getdataById(id));
    }

    @GetMapping("/all-userId")// untuk menampilkan semua data
    public CommonResponse<List<Data>> allSiswaId(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(dataService.allSiswa(userId));
    }

    @GetMapping("/getAll-guru")// untuk menampilkan semua data
    public CommonResponse<List<Data>> allGuru(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(dataService.allGuru(userId));
    }

    @GetMapping("/all-siswa")// untuk menampilkan semua data
    public CommonResponse<List<Data>> allSiswa(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(dataService.allSiswa( userId));
    }

    @GetMapping("/all-karyawan")// untuk menampilkan semua data
    public CommonResponse<List<Data>> allKaryawan(@RequestParam(name = "userId") Long userId) {
        return ResponseHelper.ok(dataService.allKaryawan(userId));
    }

    @PostMapping("post-guru") //untuk menambahkan data
    public CommonResponse<Data> addPasienGuru (PasienDto data) {
        return ResponseHelper.ok(dataService.addPasien(modelMapper.map(data, PasienDto.class)));
    }

    @PostMapping("post-siswa") //untuk menambahkan data
    public CommonResponse<Data> addPasienSiswa (PasienSiswaDto data) {
        return ResponseHelper.ok(dataService.addSiswa(modelMapper.map(data, PasienSiswaDto.class)));
    }

    @PostMapping("post-karyawan") //untuk menambahkan data
    public CommonResponse<Data> addPasienKaryawan (PasienDto data) {
        return ResponseHelper.ok(dataService.addKaryawan(modelMapper.map(data, PasienDto.class)));
    }

    @PutMapping("/{id}_guru") //untuk mengedit data per id
    public CommonResponse<Data> editGuru (@PathVariable("id") Long id, @RequestBody PasienDto data) {
        return ResponseHelper.ok(dataService.editPasien(id, data));
    }

    @PutMapping("/{id}_siswa") //untuk mengedit data per id
    public CommonResponse<Data> editSiswa (@PathVariable("id") Long id, @RequestBody PasienSiswaDto data) {
        return ResponseHelper.ok(dataService.editSiswa(id, data));
    }

    @PutMapping("/{id}_karyawan") //untuk mengedit data per id
    public CommonResponse<Data> editKaryawan(@PathVariable("id") Long id, @RequestBody PasienDto data) {
        return ResponseHelper.ok(dataService.editKaryawan(id, data));
    }

    @DeleteMapping("/{id}")// untuk deleete per id
    public CommonResponse<?> deleteById (@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataService.deletedataById(id));
    }

}
