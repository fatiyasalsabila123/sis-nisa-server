package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.DataGuru;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DataGuruService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/data-guru")
@CrossOrigin(origins = "http://localhost:3006")
public class DataGuruController {
    @Autowired
    DataGuruService dataGuruService;

    @PostMapping
    public CommonResponse<DataGuru> addguru(@RequestBody DataGuru dataGuru) {
        return ResponseHelper.ok(dataGuruService.addguru(dataGuru));
    }

    @GetMapping("/{id}")
    public CommonResponse<DataGuru> getguruById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataGuruService.getguruById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DataGuru> editguruById(@PathVariable("id") Long id,@RequestBody DataGuru dataGuru) {
        return ResponseHelper.ok(dataGuruService.editguruById(id, dataGuru));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteguruById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataGuruService.deleteguruById(id));
    }

    @GetMapping("/all-guru")
    public CommonResponse<List<DataGuru>> getAllguru() {
        return ResponseHelper.ok(dataGuruService.getAllguru());
    }
}
