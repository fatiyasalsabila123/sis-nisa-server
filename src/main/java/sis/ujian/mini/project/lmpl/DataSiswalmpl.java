package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DataSiswa;
import sis.ujian.mini.project.repository.DataSiswaRepository;
import sis.ujian.mini.project.service.DataSiswaService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataSiswalmpl implements DataSiswaService {

    @Autowired
    DataSiswaRepository dataSiswaRepository;

    @Override
    public DataSiswa addsis(DataSiswa dataSiswa) {
        DataSiswa siswa = new DataSiswa();
        siswa.setNama(dataSiswa.getNama());
        siswa.setAlamat(dataSiswa.getAlamat());
        siswa.setTanggalLahir(dataSiswa.getTanggalLahir());
        siswa.setTempatLahir(dataSiswa.getTempatLahir());
        siswa.setKelas(dataSiswa.getKelas());
        return dataSiswaRepository.save(siswa);
    }

    @Override
    public DataSiswa getsisById(Long id) {
        return dataSiswaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<DataSiswa> getAllsis() {
        return dataSiswaRepository.findAll();
    }

    @Override
    public DataSiswa editsisById(Long id, DataSiswa dataSiswa) {
        DataSiswa editSiswa = dataSiswaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        editSiswa.setNama(dataSiswa.getNama());
        editSiswa.setTanggalLahir(dataSiswa.getTanggalLahir());
        editSiswa.setTempatLahir(dataSiswa.getTempatLahir());
        editSiswa.setAlamat(dataSiswa.getAlamat());
        editSiswa.setKelas(dataSiswa.getKelas());
        return dataSiswaRepository.save(editSiswa);
    }

    @Override
    public Map<String, Boolean> deletesisById(Long id) {
        try {
            dataSiswaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
