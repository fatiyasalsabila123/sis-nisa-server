package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DaftarObat;
import sis.ujian.mini.project.model.DataGuru;
import sis.ujian.mini.project.repository.DaftarObatRepository;
import sis.ujian.mini.project.service.DaftarObatService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DaftarObatlmpl implements DaftarObatService {

    @Autowired
    DaftarObatRepository daftarObatRepository;

    @Override
    public DaftarObat addobat(DaftarObat daftarObat) {
        DaftarObat obat = new DaftarObat();
        obat.setNamaObat(daftarObat.getNamaObat());
        obat.setStock(daftarObat.getStock());
        obat.setTglExp(daftarObat.getTglExp());
        return daftarObatRepository.save(obat);
    }

    @Override
    public DaftarObat getobatById(Long id) {
        return daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<DaftarObat> getAllobat() {
        return daftarObatRepository.findAll();
    }

    @Override
    public DaftarObat editobatById(Long id, DaftarObat daftarObat) {
        DaftarObat editObat = daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        editObat.setNamaObat(daftarObat.getNamaObat());
        editObat.setStock(daftarObat.getStock());
        editObat.setTglExp(daftarObat.getTglExp());
        return daftarObatRepository.save(editObat);
    }

    @Override
    public Map<String, Boolean> deleteobatById(Long id) {
        try {
            daftarObatRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
