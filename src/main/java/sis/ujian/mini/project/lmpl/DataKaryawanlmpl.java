package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DataKaryawan;
import sis.ujian.mini.project.repository.DataKaryawanRepository;
import sis.ujian.mini.project.service.DataKaryawanService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataKaryawanlmpl implements DataKaryawanService {
    @Autowired
    DataKaryawanRepository dataKaryawanRepository;

    @Override
    public DataKaryawan addKar(DataKaryawan dataKaryawan) {
        DataKaryawan karyawan = new DataKaryawan();
        karyawan.setNama(dataKaryawan.getNama());
        karyawan.setAlamat(dataKaryawan.getAlamat());
        karyawan.setTanggalLahir(dataKaryawan.getTanggalLahir());
        karyawan.setTempatLahir(dataKaryawan.getTempatLahir());
        return dataKaryawanRepository.save(karyawan);
    }

    @Override
    public DataKaryawan getKarById(Long id) {
        return dataKaryawanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<DataKaryawan> getAllKar() {
        return dataKaryawanRepository.findAll();
    }

    @Override
    public DataKaryawan editKarById(Long id, DataKaryawan dataKaryawan) {
        DataKaryawan editKar = dataKaryawanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        editKar.setNama(dataKaryawan.getNama());
        editKar.setTanggalLahir(dataKaryawan.getTanggalLahir());
        editKar.setTempatLahir(dataKaryawan.getTempatLahir());
        editKar.setAlamat(dataKaryawan.getAlamat());
        return dataKaryawanRepository.save(editKar);
    }

    @Override
    public Map<String, Boolean> deleteKarById(Long id) {
        try {
            dataKaryawanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
