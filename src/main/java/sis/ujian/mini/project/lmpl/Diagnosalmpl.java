package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.Diagnosa;
import sis.ujian.mini.project.repository.DiagnosaRepository;
import sis.ujian.mini.project.service.DiagnosaService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Diagnosalmpl implements DiagnosaService {

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Override
    public Diagnosa adddiag(Diagnosa diagnosa) {
        Diagnosa diag = new Diagnosa();
        diag.setNamaPenyakit(diagnosa.getNamaPenyakit());
        return diagnosaRepository.save(diag);
    }

    @Override
    public Diagnosa getdiagById(Long id) {
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<Diagnosa> getAlldiag() {
        return diagnosaRepository.findAll();
    }

    @Override
    public Diagnosa editdiagById(Long id, Diagnosa diagnosa) {
        Diagnosa diag = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        diag.setNamaPenyakit(diagnosa.getNamaPenyakit());
        return diagnosaRepository.save(diag);
    }

    @Override
    public Map<String, Boolean> deletediagById(Long id) {
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
