package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DataGuru;
import sis.ujian.mini.project.repository.DataGuruRepository;
import sis.ujian.mini.project.service.DataGuruService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataGurulmpl implements DataGuruService {
    @Autowired
    DataGuruRepository dataGuruRepository;

    @Override
    public DataGuru addguru(DataGuru dataGuru) {
        DataGuru guru = new DataGuru();
        guru.setNama(dataGuru.getNama());
        guru.setAlamat(dataGuru.getAlamat());
        guru.setTanggalLahir(dataGuru.getTanggalLahir());
        guru.setTempatLahir(dataGuru.getTempatLahir());
        return dataGuruRepository.save(guru);
    }

    @Override
    public DataGuru getguruById(Long id) {
        return dataGuruRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<DataGuru> getAllguru() {
        return dataGuruRepository.findAll();
    }

    @Override
    public DataGuru editguruById(Long id, DataGuru dataGuru) {
        DataGuru editGuru = dataGuruRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        editGuru.setNama(dataGuru.getNama());
        editGuru.setTanggalLahir(dataGuru.getTanggalLahir());
        editGuru.setTempatLahir(dataGuru.getTempatLahir());
        editGuru.setAlamat(dataGuru.getAlamat());
        return dataGuruRepository.save(editGuru);
    }

    @Override
    public Map<String, Boolean> deleteguruById(Long id) {
        try {
            dataGuruRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
