package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.Registrasi;
import sis.ujian.mini.project.model.UserPrinciple;
import sis.ujian.mini.project.repository.RegistrasiRepository;

import java.util.regex.Pattern;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private RegistrasiRepository repository;

//    membuat token
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(username).matches();
        Registrasi user;
        System.out.println("is Email " + isEmail);

//        jika email ada
        if(isEmail) {
            user = repository.findByEmail(username);
        } else { // else username
            user = repository.findByUsername(username);
        }
        return UserPrinciple.build(user);
    }
}
