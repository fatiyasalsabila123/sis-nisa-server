package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.Dto.PasienDto;
import sis.ujian.mini.project.Dto.PasienSiswaDto;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.Registrasi;
import sis.ujian.mini.project.repository.DataRepository;
import sis.ujian.mini.project.service.DataService;
import sis.ujian.mini.project.service.RegistrasiService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Datalmpl implements DataService {

    @Autowired
    DataRepository dataRepository;

    @Autowired
    RegistrasiService registrasiService;

//    @Override
//    public Data adddata(Data data) {
//        Data siswa = new Data();
//        siswa.setNama(data.getNama());
//        siswa.setAlamat(data.getAlamat());
//        siswa.setTanggalLahir(data.getTanggalLahir());
//        siswa.setTempatLahir(data.getTempatLahir());
//        siswa.setJabatan(data.getJabatan());
//        return dataRepository.save(siswa);
//    }
//
    @Override
    public Data getdataById(Long id) {
        return dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }
//
//    @Override
//    public List<Data> getAlldata() {
//        return dataRepository.findAll();
//    }
//
//    @Override
//    public List<Data> findByRole(String Status) {
//        return dataRepository.findByStatus(Role.valueOf(Status)) ;
//    }
//
//    @Override
//    public Data editdataById(Long id, Data data) {
//        Data editSiswa = dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
//        editSiswa.setNama(data.getNama());
//        editSiswa.setTanggalLahir(data.getTanggalLahir());
//        editSiswa.setTempatLahir(data.getTempatLahir());
//        editSiswa.setAlamat(data.getAlamat());
//        editSiswa.setJabatan(data.getJabatan());
//        return dataRepository.save(editSiswa);
//    }

    @Override
    public Map<String, Boolean> deletedataById(Long id) {
        try {
            dataRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Data> allGuru(Long userId) {
        return dataRepository.getAllGuru(userId);
    }

    @Override
    public List<Data> allSiswa(Long userId) {
        return dataRepository.getAllSiswa(userId);
    }

    @Override
    public List<Data> allKaryawan(Long userId) {
        return dataRepository.getAllKaryawan(userId);
    }

    @Override
    public List<Data> allSiswaId(Long userId) {
        return dataRepository.getAllUserId(userId);
    }

    @Override
    public Data addPasien(PasienDto pasienDto) {
        Data pasien = new Data();
        Registrasi register = registrasiService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("guru");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        return dataRepository.save(pasien);

    }

    @Override
    public Data addSiswa(PasienSiswaDto pasienSiswaDto) {
        Data pasien = new Data();
        Registrasi register = registrasiService.getById(pasienSiswaDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienSiswaDto.getUsername());
        pasien.setJabatan(pasienSiswaDto.getJabatan());
        pasien.setStatus("siswa");
        pasien.setTempat(pasienSiswaDto.getTempat());
        pasien.setTanggalLahir(pasienSiswaDto.getTanggalLahir());
        pasien.setAlamat(pasienSiswaDto.getAlamat());
        pasien.setKelas(pasienSiswaDto.getKelas());
        return dataRepository.save(pasien);
    }

    @Override
    public Data addKaryawan(PasienDto pasienDto) {
        Data pasien = new Data();
        Registrasi register = registrasiService.getById(pasienDto.getUserId());
        pasien.setSiswaId(register);
        pasien.setUsername(pasienDto.getUsername());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setStatus("karyawan");
        pasien.setTempat(pasienDto.getTempat());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setAlamat(pasienDto.getAlamat());
        return dataRepository.save(pasien);
    }

    @Override
    public Data editPasien(Long id, PasienDto pasienDto) {
        Data pasien = dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return dataRepository.save(pasien);
    }

    @Override
    public Data editSiswa(Long id, PasienSiswaDto pasienSiswaDto) {
        Data pasien = dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienSiswaDto.getUsername());
        pasien.setTanggalLahir(pasienSiswaDto.getTanggalLahir());
        pasien.setJabatan(pasienSiswaDto.getJabatan());
        pasien.setTempat(pasienSiswaDto.getTempat());
        pasien.setAlamat(pasienSiswaDto.getAlamat());
        pasien.setKelas(pasienSiswaDto.getKelas());
        return dataRepository.save(pasien);
    }

    @Override
    public Data editKaryawan(Long id, PasienDto pasienDto) {
        Data pasien = dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        pasien.setUsername(pasienDto.getUsername());
        pasien.setTanggalLahir(pasienDto.getTanggalLahir());
        pasien.setJabatan(pasienDto.getJabatan());
        pasien.setTempat(pasienDto.getTempat());
        pasien.setAlamat(pasienDto.getAlamat());
        return dataRepository.save(pasien);
    }


}
