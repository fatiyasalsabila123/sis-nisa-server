package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DaftarPasien;
import sis.ujian.mini.project.repository.*;
import sis.ujian.mini.project.service.DaftarPasienService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DaftarPasienlmpl implements DaftarPasienService {

    @Autowired
    DaftarPasienRepository daftarPasienRepository;

    @Autowired
    DataRepository dataRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
    public DaftarPasien addpasien(PeriksaPasienDto daftarPasien) {
        DaftarPasien periksaPasien1 = new DaftarPasien();
        periksaPasien1.setKeluhan(daftarPasien.getKeluhan());
        periksaPasien1.setStatusPasien(daftarPasien.getStatusPasien());
        periksaPasien1.setStatus("belum ditangani");
        periksaPasien1.setNamaPasien(dataRepository.findById(daftarPasien.getNamaPasien()).orElseThrow(() -> new NotFoundException("id not found")));
        return daftarPasienRepository.save(periksaPasien1);

    }

    @Override
    public DaftarPasien getpasienById(Long id) {
        return daftarPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<DaftarPasien> getAllpasien() {
        return daftarPasienRepository.findAll();
    }

    @Override
    public DaftarPasien editpasienById(Long id, TanganiDto tanganiDto) {
        DaftarPasien periksaPasien = daftarPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        periksaPasien.setStatus("sudah ditangani");
        periksaPasien.setPenyakitId(diagnosaRepository.findById(tanganiDto.getPanyakitId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setPenangananId(penangananRepository.findById(tanganiDto.getPenangananId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setTindakanId(tindakanRepository.findById(tanganiDto.getTindakanId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setKeluhan(tanganiDto.getKeluhan());
        return daftarPasienRepository.save(periksaPasien);

    }

    @Override
    public Map<String, Boolean> deletepasienById(Long id) {
        try {
            daftarPasienRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
