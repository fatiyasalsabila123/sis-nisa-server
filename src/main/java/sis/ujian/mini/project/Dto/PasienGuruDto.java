package sis.ujian.mini.project.Dto;

import java.util.Date;

public class PasienGuruDto {

    private String namaGuru;

    private  String jabatan;

    private String tanggalLahirGuru;

    private String tempatGuru;

    private Date waktuGuru;

    private Long userId;

    public String getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(String namaGuru) {
        this.namaGuru = namaGuru;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getTanggalLahirGuru() {
        return tanggalLahirGuru;
    }

    public void setTanggalLahirGuru(String tanggalLahirGuru) {
        this.tanggalLahirGuru = tanggalLahirGuru;
    }

    public String getTempatGuru() {
        return tempatGuru;
    }

    public void setTempatGuru(String tempatGuru) {
        this.tempatGuru = tempatGuru;
    }

    public Date getWaktuGuru() {
        return waktuGuru;
    }

    public void setWaktuGuru(Date waktuGuru) {
        this.waktuGuru = waktuGuru;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
